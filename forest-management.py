# https://pymdptoolbox.readthedocs.io/en/latest/api/example.html#mdptoolbox.example.forest


# import mdptoolbox.example
# P, R = mdptoolbox.example.forest()
# P
# # array([[[ 0.1,  0.9,  0. ],
# #         [ 0.1,  0. ,  0.9],
# #         [ 0.1,  0. ,  0.9]],

# #        [[ 1. ,  0. ,  0. ],
# #         [ 1. ,  0. ,  0. ],
# #         [ 1. ,  0. ,  0. ]]])
# R
# # array([[ 0.,  0.],
# #        [ 0.,  1.],
# #        [ 4.,  2.]])
# Psp, Rsp = mdptoolbox.example.forest(is_sparse=True)
# len(Psp)
# # 2
# Psp[0]
# # <3x3 sparse matrix of type '<... 'numpy.float64'>'
# #     with 6 stored elements in Compressed Sparse Row format>
# Psp[1]
# # <3x3 sparse matrix of type '<... 'numpy.int64'>'
# #     with 3 stored elements in Compressed Sparse Row format>
# Rsp
# # array([[ 0.,  0.],
# #        [ 0.,  1.],
# #        [ 4.,  2.]])
# (Psp[0].todense() == P[0]).all()
# # True
# (Rsp == R).all()
# # True

# https://github.com/hiive/hiivemdptoolbox
from hiive import mdptoolbox
from hiive.mdptoolbox import mdp
import hiive.mdptoolbox.example

import json

P, R = mdptoolbox.example.forest(S=3, r1=.5, r2=.5, p=0.02)
vi = mdptoolbox.mdp.ValueIteration(P, R, 0.9, max_iter=10000, run_stat_frequency=1)
vi.run()
print(vi.policy) # result is (0, 0, 0)

json = json.dumps(vi.run_stats, indent=2)

print(json)


# https://stackoverflow.com/questions/10194482/custom-matplotlib-plot-chess-board-like-table-with-colored-cells
