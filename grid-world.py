from hiive import mdptoolbox
from hiive.mdptoolbox import mdp
import hiive.mdptoolbox.example

import json

import numpy as np
# set random seed
# np.random.seed(55)

# create random MDP
# P, R = mdptoolbox.example.rand(S=10, A=3)

# https://stats.stackexchange.com/questions/339592/how-to-get-p-and-r-values-for-a-markov-decision-process-grid-world-problem
def run(func):
  func()
  return func

def grid_world_example(grid_size=(3, 4),
                       black_cells=[(1,1)],
                       white_cell_reward=-0.02,
                       green_cell_loc=(0,3),
                       red_cell_loc=(1,3),
                       green_cell_reward=1.0,
                       red_cell_reward=-1.0,
                       action_lrfb_prob=(.1, .1, .8, 0.),
                       start_loc=(0, 0)
                      ):
    num_states = grid_size[0] * grid_size[1]
    num_actions = 4
    P = np.zeros((num_actions, num_states, num_states))
    R = np.zeros((num_states, num_actions))

    @run
    def fill_in_probs():
        # helpers
        to_2d = lambda x: np.unravel_index(x, grid_size)
        to_1d = lambda x: np.ravel_multi_index(x, grid_size)

        def hit_wall(cell):
            if cell in black_cells:
                return True
            try: # ...good enough...
                to_1d(cell)
            except ValueError as e:
                return True
            return False

        # make probs for each action
        a_up = [action_lrfb_prob[i] for i in (0, 1, 2, 3)]
        a_down = [action_lrfb_prob[i] for i in (1, 0, 3, 2)]
        a_left = [action_lrfb_prob[i] for i in (2, 3, 1, 0)]
        a_right = [action_lrfb_prob[i] for i in (3, 2, 0, 1)]
        actions = [a_up, a_down, a_left, a_right]
        for i, a in enumerate(actions):
            actions[i] = {'up':a[2], 'down':a[3], 'left':a[0], 'right':a[1]}

        # work in terms of the 2d grid representation

        def update_P_and_R(cell, new_cell, a_index, a_prob):
            if cell == green_cell_loc:
                P[a_index, to_1d(cell), to_1d(cell)] = 1.0
                R[to_1d(cell), a_index] = green_cell_reward

            elif cell == red_cell_loc:
                P[a_index, to_1d(cell), to_1d(cell)] = 1.0
                R[to_1d(cell), a_index] = red_cell_reward

            elif hit_wall(new_cell):  # add prob to current cell
                P[a_index, to_1d(cell), to_1d(cell)] += a_prob
                R[to_1d(cell), a_index] = white_cell_reward

            else:
                P[a_index, to_1d(cell), to_1d(new_cell)] = a_prob
                R[to_1d(cell), a_index] = white_cell_reward

        for a_index, action in enumerate(actions):
            for cell in np.ndindex(grid_size):
                # up
                new_cell = (cell[0]-1, cell[1])
                update_P_and_R(cell, new_cell, a_index, action['up'])

                # down
                new_cell = (cell[0]+1, cell[1])
                update_P_and_R(cell, new_cell, a_index, action['down'])

                # left
                new_cell = (cell[0], cell[1]-1)
                update_P_and_R(cell, new_cell, a_index, action['left'])

                # right
                new_cell = (cell[0], cell[1]+1)
                update_P_and_R(cell, new_cell, a_index, action['right'])

    return P, R

'''
P = transition probability matrices
R = reward matrices or vectors
'''

P, R = grid_world_example()
# 0=up, 1=down, 2=left, 3=right
# VI = [
# R, R, R, U,
#  U, U, U, U,
#   U, L, U, L]

# print('P')
# print(P)

# print('R')
# print(R)

print('')
print('=== START VI ===')

vi = mdptoolbox.mdp.ValueIteration(P, R, 0.9, max_iter=10000, run_stat_frequency=1)
vi.run()

print('value iteration policy')
print(vi.policy)
# (3, 3, 3, 0, 0, 0, 0, 0, 0, 2, 0, 2)

# print('value iteration policy stats')
# print(vi.run_stats)
# # [{'State': None, 'Action': None, 'Reward': 1.0, 'Error': 2.0, 'Time': 3.695487976074219e-05, 'Max V': 1.0, 'Mean V': -0.016666666666666673, 'Iteration': 1}, {'State': None, 'Action': None, 'Reward': 1.9, 'Error': 1.7999999999999998, 'Time': 8.487701416015625e-05, 'Max V': 1.9, 'Mean V': 0.02953333333333334, 'Iteration': 2}, {'State': None, 'Action': None, 'Reward': 2.71, 'Error': 1.62, 'Time': 0.0001399517059326172, 'Max V': 2.71, 'Mean V': 0.15078433333333333, 'Iteration': 3}, {'State': None, 'Action': None, 'Reward': 3.439, 'Error': 1.4580000000000002, 'Time': 0.00018310546875, 'Max V': 3.439, 'Mean V': 0.37613416333333327, 'Iteration': 4}, {'State': None, 'Action': None, 'Reward': 4.0951, 'Error': 1.3122000000000007, 'Time': 0.00022363662719726562, 'Max V': 4.0951, 'Mean V': 0.6595343227333333, 'Iteration': 5}, {'State': None, 'Action': None, 'Reward': 4.68559, 'Error': 1.18098, 'Time': 0.0002636909484863281, 'Max V': 4.68559, 'Mean V': 0.9770512907083336, 'Iteration': 6}, {'State': None, 'Action': None, 'Reward': 5.217031, 'Error': 1.062882, 'Time': 0.0003032684326171875, 'Max V': 5.217031, 'Mean V': 1.3083064973808136, 'Iteration': 7}, {'State': None, 'Action': None, 'Reward': 5.6953279000000006, 'Error': 0.9565938000000003, 'Time': 0.00034236907958984375, 'Max V': 5.6953279000000006, 'Mean V': 1.6261798689932572, 'Iteration': 8}, {'State': None, 'Action': None, 'Reward': 6.12579511, 'Error': 0.8609344199999995, 'Time': 0.0003819465637207031, 'Max V': 6.12579511, 'Mean V': 1.9224187652762597, 'Iteration': 9}, {'State': None, 'Action': None, 'Reward': 6.5132155990000005, 'Error': 0.7748409780000003, 'Time': 0.0004208087921142578, 'Max V': 6.5132155990000005, 'Mean V': 2.1935360943342395, 'Iteration': 10}, {'State': None, 'Action': None, 'Reward': 6.861894039100001, 'Error': 0.697356880200001, 'Time': 0.00046563148498535156, 'Max V': 6.861894039100001, 'Mean V': 2.439625413967356, 'Iteration': 11}, {'State': None, 'Action': None, 'Reward': 7.175704635190001, 'Error': 0.6276211921799995, 'Time': 0.0005099773406982422, 'Max V': 7.175704635190001, 'Mean V': 2.662008928361997, 'Iteration': 12}, {'State': None, 'Action': None, 'Reward': 7.458134171671, 'Error': 0.5648590729619993, 'Time': 0.0005483627319335938, 'Max V': 7.458134171671, 'Mean V': 2.862552634892561, 'Iteration': 13}, {'State': None, 'Action': None, 'Reward': 7.7123207545039, 'Error': 0.5083731656657999, 'Time': 0.0005867481231689453, 'Max V': 7.7123207545039, 'Mean V': 3.0432124996667986, 'Iteration': 14}, {'State': None, 'Action': None, 'Reward': 7.94108867905351, 'Error': 0.4575358490992194, 'Time': 0.0006251335144042969, 'Max V': 7.94108867905351, 'Mean V': 3.205879944741558, 'Iteration': 15}, {'State': None, 'Action': None, 'Reward': 8.14697981114816, 'Error': 0.4117822641893003, 'Time': 0.0006635189056396484, 'Max V': 8.14697981114816, 'Mean V': 3.352311906789209, 'Iteration': 16}, {'State': None, 'Action': None, 'Reward': 8.332281830033345, 'Error': 0.3706040377703701, 'Time': 0.000701904296875, 'Max V': 8.332281830033345, 'Mean V': 3.4841140117603775, 'Iteration': 17}, {'State': None, 'Action': None, 'Reward': 8.49905364703001, 'Error': 0.33354363399332954, 'Time': 0.0007407665252685547, 'Max V': 8.49905364703001, 'Mean V': 3.6027415570498884, 'Iteration': 18}, {'State': None, 'Action': None, 'Reward': 8.649148282327008, 'Error': 0.3001892705939966, 'Time': 0.0007791519165039062, 'Max V': 8.649148282327008, 'Mean V': 3.7095087476251796, 'Iteration': 19}, {'State': None, 'Action': None, 'Reward': 8.784233454094307, 'Error': 0.27017034353459835, 'Time': 0.0008175373077392578, 'Max V': 8.784233454094307, 'Mean V': 3.8056002351935203, 'Iteration': 20}, {'State': None, 'Action': None, 'Reward': 8.905810108684877, 'Error': 0.24315330918113887, 'Time': 0.0008592605590820312, 'Max V': 8.905810108684877, 'Mean V': 3.89208300501078, 'Iteration': 21}, {'State': None, 'Action': None, 'Reward': 9.01522909781639, 'Error': 0.21883797826302498, 'Time': 0.0008983612060546875, 'Max V': 9.01522909781639, 'Mean V': 3.9699176805301395, 'Iteration': 22}, {'State': None, 'Action': None, 'Reward': 9.113706188034751, 'Error': 0.1969541804367232, 'Time': 0.0009369850158691406, 'Max V': 9.113706188034751, 'Mean V': 4.039968966064247, 'Iteration': 23}, {'State': None, 'Action': None, 'Reward': 9.202335569231277, 'Error': 0.17725876239305194, 'Time': 0.0009806156158447266, 'Max V': 9.202335569231277, 'Mean V': 4.103015155992984, 'Iteration': 24}, {'State': None, 'Action': None, 'Reward': 9.28210201230815, 'Error': 0.1595328861537446, 'Time': 0.0010192394256591797, 'Max V': 9.28210201230815, 'Mean V': 4.159756740948181, 'Iteration': 25}, {'State': None, 'Action': None, 'Reward': 9.353891811077334, 'Error': 0.1435795975383698, 'Time': 0.0010581016540527344, 'Max V': 9.353891811077334, 'Mean V': 4.210824173379313, 'Iteration': 26}, {'State': None, 'Action': None, 'Reward': 9.4185026299696, 'Error': 0.1292216377845321, 'Time': 0.001096487045288086, 'Max V': 9.4185026299696, 'Mean V': 4.25678486511514, 'Iteration': 27}, {'State': None, 'Action': None, 'Reward': 9.47665236697264, 'Error': 0.11629947400608032, 'Time': 0.001134634017944336, 'Max V': 9.47665236697264, 'Mean V': 4.2981494887659, 'Iteration': 28}, {'State': None, 'Action': None, 'Reward': 9.528987130275377, 'Error': 0.10466952660547335, 'Time': 0.0011725425720214844, 'Max V': 9.528987130275377, 'Mean V': 4.3357945572904155, 'Iteration': 29}, {'State': None, 'Action': None, 'Reward': 9.57608841724784, 'Error': 0.09420257394492637, 'Time': 0.001211404800415039, 'Max V': 9.57608841724784, 'Mean V': 4.370128799545465, 'Iteration': 30}, {'State': None, 'Action': None, 'Reward': 9.618479575523056, 'Error': 0.08478231655043089, 'Time': 0.0012500286102294922, 'Max V': 9.618479575523056, 'Mean V': 4.401223335115403, 'Iteration': 31}, {'State': None, 'Action': None, 'Reward': 9.656631617970751, 'Error': 0.07630408489539064, 'Time': 0.0012943744659423828, 'Max V': 9.656631617970751, 'Mean V': 4.429302414927649, 'Iteration': 32}, {'State': None, 'Action': None, 'Reward': 9.690968456173676, 'Error': 0.06867367640585087, 'Time': 0.001332998275756836, 'Max V': 9.690968456173676, 'Mean V': 4.454611902009826, 'Iteration': 33}, {'State': None, 'Action': None, 'Reward': 9.72187161055631, 'Error': 0.06180630876526649, 'Time': 0.001371145248413086, 'Max V': 9.72187161055631, 'Mean V': 4.477406943709217, 'Iteration': 34}, {'State': None, 'Action': None, 'Reward': 9.749684449500679, 'Error': 0.05562567788873807, 'Time': 0.001409769058227539, 'Max V': 9.749684449500679, 'Mean V': 4.49792917052241, 'Iteration': 35}, {'State': None, 'Action': None, 'Reward': 9.77471600455061, 'Error': 0.050063110099863195, 'Time': 0.0014481544494628906, 'Max V': 9.77471600455061, 'Mean V': 4.5164019192629326, 'Iteration': 36}, {'State': None, 'Action': None, 'Reward': 9.797244404095549, 'Error': 0.045056799089877586, 'Time': 0.0014920234680175781, 'Max V': 9.797244404095549, 'Mean V': 4.533028495610572, 'Iteration': 37}, {'State': None, 'Action': None, 'Reward': 9.817519963685994, 'Error': 0.04055111918088983, 'Time': 0.0015304088592529297, 'Max V': 9.817519963685994, 'Mean V': 4.547992857493093, 'Iteration': 38}, {'State': None, 'Action': None, 'Reward': 9.835767967317395, 'Error': 0.036496007262801555, 'Time': 0.0015685558319091797, 'Max V': 9.835767967317395, 'Mean V': 4.561460959868495, 'Iteration': 39}, {'State': None, 'Action': None, 'Reward': 9.852191170585655, 'Error': 0.032846406536521044, 'Time': 0.0016067028045654297, 'Max V': 9.852191170585655, 'Mean V': 4.573582322343179, 'Iteration': 40}, {'State': None, 'Action': None, 'Reward': 9.86697205352709, 'Error': 0.02956176588287107, 'Time': 0.0016450881958007812, 'Max V': 9.86697205352709, 'Mean V': 4.5844915764698, 'Iteration': 41}, {'State': None, 'Action': None, 'Reward': 9.880274848174382, 'Error': 0.0266055892945829, 'Time': 0.0016834735870361328, 'Max V': 9.880274848174382, 'Mean V': 4.594309916234874, 'Iteration': 42}, {'State': None, 'Action': None, 'Reward': 9.892247363356944, 'Error': 0.023945030365123188, 'Time': 0.0017213821411132812, 'Max V': 9.892247363356944, 'Mean V': 4.603146426392995, 'Iteration': 43}, {'State': None, 'Action': None, 'Reward': 9.90302262702125, 'Error': 0.021550527328610514, 'Time': 0.0017595291137695312, 'Max V': 9.90302262702125, 'Mean V': 4.611099287261328, 'Iteration': 44}, {'State': None, 'Action': None, 'Reward': 9.912720364319124, 'Error': 0.019395474595750528, 'Time': 0.0017981529235839844, 'Max V': 9.912720364319124, 'Mean V': 4.618256862723971, 'Iteration': 45}, {'State': None, 'Action': None, 'Reward': 9.921448327887212, 'Error': 0.01745592713617583, 'Time': 0.0018448829650878906, 'Max V': 9.921448327887212, 'Mean V': 4.624698680908987, 'Iteration': 46}, {'State': None, 'Action': None, 'Reward': 9.929303495098491, 'Error': 0.015710334422557537, 'Time': 0.0018832683563232422, 'Max V': 9.929303495098491, 'Mean V': 4.6304963173813904, 'Iteration': 47}, {'State': None, 'Action': None, 'Reward': 9.936373145588643, 'Error': 0.014139300980303915, 'Time': 0.0019214153289794922, 'Max V': 9.936373145588643, 'Mean V': 4.635714190248276, 'Iteration': 48}, {'State': None, 'Action': None, 'Reward': 9.942735831029779, 'Error': 0.012725370882272102, 'Time': 0.001959562301635742, 'Max V': 9.942735831029779, 'Mean V': 4.64041027584491, 'Iteration': 49}, {'State': None, 'Action': None, 'Reward': 9.948462247926802, 'Error': 0.011452833794045603, 'Time': 0.0020034313201904297, 'Max V': 9.948462247926802, 'Mean V': 4.644636752888352, 'Iteration': 50}, {'State': None, 'Action': None, 'Reward': 9.953616023134122, 'Error': 0.010307550414641042, 'Time': 0.0020418167114257812, 'Max V': 9.953616023134122, 'Mean V': 4.6484405822299975, 'Iteration': 51}, {'State': None, 'Action': None, 'Reward': 9.95825442082071, 'Error': 0.009276795373175162, 'Time': 0.002080202102661133, 'Max V': 9.95825442082071, 'Mean V': 4.6518640286384825, 'Iteration': 52}, {'State': None, 'Action': None, 'Reward': 9.96242897873864, 'Error': 0.008349115835859067, 'Time': 0.0021185874938964844, 'Max V': 9.96242897873864, 'Mean V': 4.654945130406515, 'Iteration': 53}, {'State': None, 'Action': None, 'Reward': 9.966186080864777, 'Error': 0.007514204252274226, 'Time': 0.002156496047973633, 'Max V': 9.966186080864777, 'Mean V': 4.657718121997898, 'Iteration': 54}, {'State': None, 'Action': None, 'Reward': 9.9695674727783, 'Error': 0.006762783827046093, 'Time': 0.0021948814392089844, 'Max V': 9.9695674727783, 'Mean V': 4.660213814430205, 'Iteration': 55}, {'State': None, 'Action': None, 'Reward': 9.97261072550047, 'Error': 0.006086505444340418, 'Time': 0.0022330284118652344, 'Max V': 9.97261072550047, 'Mean V': 4.662459937619304, 'Iteration': 56}, {'State': None, 'Action': None, 'Reward': 9.975349652950424, 'Error': 0.005477854899908152, 'Time': 0.0022711753845214844, 'Max V': 9.975349652950424, 'Mean V': 4.6644814484895045, 'Iteration': 57}, {'State': None, 'Action': None, 'Reward': 9.977814687655382, 'Error': 0.004930069409915916, 'Time': 0.002309560775756836, 'Max V': 9.977814687655382, 'Mean V': 4.666300808272687, 'Iteration': 58}, {'State': None, 'Action': None, 'Reward': 9.980033218889844, 'Error': 0.004437062468923614, 'Time': 0.002347707748413086, 'Max V': 9.980033218889844, 'Mean V': 4.667938232077554, 'Iteration': 59}, {'State': None, 'Action': None, 'Reward': 9.98202989700086, 'Error': 0.0039933562220326735, 'Time': 0.002386808395385742, 'Max V': 9.98202989700086, 'Mean V': 4.669411913501935, 'Iteration': 60}, {'State': None, 'Action': None, 'Reward': 9.983826907300774, 'Error': 0.0035940205998272745, 'Time': 0.0024251937866210938, 'Max V': 9.983826907300774, 'Mean V': 4.670738226783876, 'Iteration': 61}, {'State': None, 'Action': None, 'Reward': 9.985444216570697, 'Error': 0.003234618539845968, 'Time': 0.0024635791778564453, 'Max V': 9.985444216570697, 'Mean V': 4.671931908737624, 'Iteration': 62}, {'State': None, 'Action': None, 'Reward': 9.986899794913628, 'Error': 0.0029111566858617266, 'Time': 0.0025072097778320312, 'Max V': 9.986899794913628, 'Mean V': 4.673006222495998, 'Iteration': 63}, {'State': None, 'Action': None, 'Reward': 9.988209815422264, 'Error': 0.0026200410172734223, 'Time': 0.002545595169067383, 'Max V': 9.988209815422264, 'Mean V': 4.673973104878534, 'Iteration': 64}, {'State': None, 'Action': None, 'Reward': 9.989388833880039, 'Error': 0.0023580369155489223, 'Time': 0.002583742141723633, 'Max V': 9.989388833880039, 'Mean V': 4.674843299022817, 'Iteration': 65}, {'State': None, 'Action': None, 'Reward': 9.990449950492035, 'Error': 0.002122233223992964, 'Time': 0.002621889114379883, 'Max V': 9.990449950492035, 'Mean V': 4.675626473752671, 'Iteration': 66}, {'State': None, 'Action': None, 'Reward': 9.991404955442832, 'Error': 0.0019100099015929572, 'Time': 0.0026602745056152344, 'Max V': 9.991404955442832, 'Mean V': 4.67633133100954, 'Iteration': 67}, {'State': None, 'Action': None, 'Reward': 9.992264459898548, 'Error': 0.001719008911432951, 'Time': 0.002699136734008789, 'Max V': 9.992264459898548, 'Mean V': 4.6769657025407225, 'Iteration': 68}, {'State': None, 'Action': None, 'Reward': 9.993038013908693, 'Error': 0.0015471080202900112, 'Time': 0.002737283706665039, 'Max V': 9.993038013908693, 'Mean V': 4.677536636918785, 'Iteration': 69}, {'State': None, 'Action': None, 'Reward': 9.993734212517824, 'Error': 0.00139239721826101, 'Time': 0.0027756690979003906, 'Max V': 9.993734212517824, 'Mean V': 4.678050477859043, 'Iteration': 70}, {'State': None, 'Action': None, 'Reward': 9.994360791266041, 'Error': 0.0012531574964356196, 'Time': 0.002814054489135742, 'Max V': 9.994360791266041, 'Mean V': 4.6785129347052745, 'Iteration': 71}, {'State': None, 'Action': None, 'Reward': 9.994924712139438, 'Error': 0.0011278417467934787, 'Time': 0.0028543472290039062, 'Max V': 9.994924712139438, 'Mean V': 4.678929145866883, 'Iteration': 72}]

print(f'vi iterations: [{len(vi.run_stats)}]')
# vi iterations: [72]

vi_total_time = 0
vi_iteration_list = []
vi_mean_v_list = []
for i in vi.run_stats:
  vi_total_time = vi_total_time + i['Time']
  vi_iteration_list.append(i['Iteration'])
  vi_mean_v_list.append(i['Mean V'])

print(f'vi_total_time: {vi_total_time}')
# vi_total_time: 0.1025238037109375
print(f'vi_iteration_list: {vi_iteration_list}')
# vi_iteration_list: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72]
print(f'vi_mean_v_list: {vi_mean_v_list}')
# vi_mean_v_list: [-0.016666666666666673, 0.02953333333333334, 0.15078433333333333, 0.37613416333333327, 0.6595343227333333, 0.9770512907083336, 1.3083064973808136, 1.6261798689932572, 1.9224187652762597, 2.1935360943342395, 2.439625413967356, 2.662008928361997, 2.862552634892561, 3.0432124996667986, 3.205879944741558, 3.352311906789209, 3.4841140117603775, 3.6027415570498884, 3.7095087476251796, 3.8056002351935203, 3.89208300501078, 3.9699176805301395, 4.039968966064247, 4.103015155992984, 4.159756740948181, 4.210824173379313, 4.25678486511514, 4.2981494887659, 4.3357945572904155, 4.370128799545465, 4.401223335115403, 4.429302414927649, 4.454611902009826, 4.477406943709217, 4.49792917052241, 4.5164019192629326, 4.533028495610572, 4.547992857493093, 4.561460959868495, 4.573582322343179, 4.5844915764698, 4.594309916234874, 4.603146426392995, 4.611099287261328, 4.618256862723971, 4.624698680908987, 4.6304963173813904, 4.635714190248276, 4.64041027584491, 4.644636752888352, 4.6484405822299975, 4.6518640286384825, 4.654945130406515, 4.657718121997898, 4.660213814430205, 4.662459937619304, 4.6644814484895045, 4.666300808272687, 4.667938232077554, 4.669411913501935, 4.670738226783876, 4.671931908737624, 4.673006222495998, 4.673973104878534, 4.674843299022817, 4.675626473752671, 4.67633133100954, 4.6769657025407225, 4.677536636918785, 4.678050477859043, 4.6785129347052745, 4.678929145866883]

print('=== END VI ===')

print('')

print('=== START PI ===')

pi = mdptoolbox.mdp.PolicyIteration(P, R, 0.9, max_iter=10000, run_stat_frequency=1)
pi.run()

print('policy iteration policy')
print(pi.policy)
# (3, 3, 3, 0, 0, 0, 0, 0, 0, 2, 0, 2)

# print('policy iteration policy stats')
# print(pi.run_stats)
# [{'State': None, 'Action': None, 'Reward': 10.000000000000002, 'Error': 6.662379192968298, 'Time': 0.00048160552978515625, 'Max V': 10.000000000000002, 'Mean V': 0.1384058815117256, 'Iteration': 1}, {'State': None, 'Action': None, 'Reward': 10.000000000000005, 'Error': 1.2187442861887154, 'Time': 0.0007498264312744141, 'Max V': 10.000000000000005, 'Mean V': 4.568869085931863, 'Iteration': 2}, {'State': None, 'Action': None, 'Reward': 10.000000000000005, 'Error': 1.7763568394002505e-15, 'Time': 0.0009963512420654297, 'Max V': 10.000000000000005, 'Mean V': 4.68267504632136, 'Iteration': 3}]

print(f'pi iterations: [{len(pi.run_stats)}]')
# pi iterations: [3]

pi_total_time = 0
pi_iteration_list = []
pi_mean_v_list = []
for i in pi.run_stats:
  pi_total_time = pi_total_time + i['Time']
  pi_iteration_list.append(i['Iteration'])
  pi_mean_v_list.append(i['Mean V'])

print(f'pi_total_time: {pi_total_time}')
# pi_total_time: 0.0022046566009521484
print(f'pi_iteration_list: {pi_iteration_list}')
# pi_iteration_list: [1, 2, 3]
print(f'pi_mean_v_list: {pi_mean_v_list}')
# pi_mean_v_list: [0.1384058815117256, 4.568869085931863, 4.68267504632136]

print('=== END PI ===')
print('')
