# CS7641 Project 4
Matthew Guillet

Code: https://bitbucket.org/mmguillet/cs7641-project4/src/master/
Visualizations (2 tabs): https://docs.google.com/spreadsheets/d/1N_7grZHeGi7YBMKG5dx7SapG8LQ4-21UPykc8LkvTOI/edit?usp=sharing

**Note: Thank you for your time, this report will be short. I spent as much time as I could exploring MDPs, unfortunately I had to devote the majority of the last ~3 weeks to support the biggest software launch of my career.

## Install and Run
```
# clone repo
git clone git@bitbucket.org:mmguillet/cs7641-project4.git

# go into project directory
cd cs7641-project4

# install python virtual environment
sudo apt install python3-venv

# setup new python virtual environment
python3 -m venv venv

# activate virtual environment
source venv/bin/activate

# update python module installer modules
pip install -U setuptools pip

# install python modules
pip install -r requirements.txt

# run code
python3 grid-world.py
```

Sources:
https://pymdptoolbox.readthedocs.io/
https://stackoverflow.com/
https://stats.stackexchange.com

